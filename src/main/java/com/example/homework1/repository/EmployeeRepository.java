package com.example.homework1.repository;

import com.example.homework1.entity.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Employee employee){

        em.persist(employee);
//        em.detach(employee);
//        em.merge(employee);
//        em.flush();
//
//        System.out.println("gg:"+em.contains(employee));
//        em.persist(employee);
//        Employee gg = em.find(Employee.class,employee.getId());
//        System.out.println("DD:"+em.contains(gg));
//
//        em.detach(gg);
//        System.out.println("DgggD:"+em.contains(gg));
    }

    @Transactional
    public void deatch(Employee employee){
        em.detach(employee);
    }

    @Transactional
    public void mearge(Employee employee){

        employee.setLastName("Vik");
        em.merge(employee);
        em.flush();
    }

//    @Transactional
//    public void flush(){
//        em.flush();
//    }

    @Transactional
    public void remove(Employee employee){
        em.remove(em.contains(employee) ? employee : em.merge(employee));
    }

    @Transactional
    public String findById(Long id){
      Employee employee = em.find(Employee.class, id);

      if(employee == null){
          return "Employee Not Found";
      }else
          return employee.getLastName();
    }

}
