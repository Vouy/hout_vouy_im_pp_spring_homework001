package com.example.homework1;

import com.example.homework1.entity.Employee;
import com.example.homework1.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.*;
import java.sql.Date;

@SpringBootApplication
@RestController
@RequestMapping("")
@AllArgsConstructor
@Data
public class Homework1Application {

    private final EmployeeRepository employeeRepository;

   static Employee  employee = new Employee("Hout","Vouy Im","vouyim@gmail.com", new Date(System.currentTimeMillis()));
    @GetMapping("/insert-data")
    public String insertData(){
//        Employee  employee = new Employee("Hout","Vouy Im","vouyim@gmail.com", new Date(System.currentTimeMillis()));
            employeeRepository.insert(employee);
        return "Insert Success";
    }

    @GetMapping("/detach-data")
    public String detachData(){
//        Employee  employee = new Employee("Hout","Vouy Im","vouyim@gmail.com", new Date(System.currentTimeMillis()));
        employeeRepository.deatch(employee);
        return "Deatch Success";
    }

    @GetMapping("/mearge-data")
    public String meargeData(){
//        Employee  employee = new Employee("Hout","Vouy Im","vouyim@gmail.com", new Date(System.currentTimeMillis()));
        employeeRepository.mearge(employee);
        return "Mearge Success";
    }

//    @GetMapping("/flush-data")
//    public String flushData(){
//        employeeRepository.flush();
//        return "Flush Success";
//    }

    @GetMapping("/remove-data")
    public String removeData(){
        employeeRepository.remove(employee);
        return "Remove Success";
    }

    @GetMapping("/find-employee-by-id/{id}")
    public String findEmployeeById(@PathVariable Long id){
        return employeeRepository.findById(id);
//        return employee.getLastName();
    }

    public static void main(String[] args) {
        SpringApplication.run(Homework1Application.class, args);
    }



}
