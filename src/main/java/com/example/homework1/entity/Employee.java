package com.example.homework1.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Entity
@NoArgsConstructor
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

   @Column(nullable = false , unique = true)
    //@Column(nullable = false)
    private String email; // email should be unique

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date bithDate; // the date has format only date: 2023-08-28

    @Transient
    private String temp; // for temp field should be don't persist data or appear in table column

    public Employee(String firstName, String lastName, String email, Date bithDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.bithDate = bithDate;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Date getBithDate() {
        return bithDate;
    }

}
